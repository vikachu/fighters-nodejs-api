const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action
        const data = AuthService.login(req.body);
        res.data = data;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;