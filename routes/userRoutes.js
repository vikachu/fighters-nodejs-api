const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function (req, res, next) {
    try {
        const result = UserService.getAll();
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.get('/:id', function (req, res, next) {
    try {
        const result = UserService.getOne(req.params.id);
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.post('/', createUserValid, function (req, res, next) {
    try {
        const result = UserService.create(req.body);
        res.data = result;
        res.statusCode = 200;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.put('/:id', updateUserValid, function (req, res, next) {
    try {
        const result = UserService.update(req.params.id, req.body);
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
    try {
        const result = UserService.delete(req.params.id);
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

module.exports = router;