const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function (req, res, next) {
    try {
        const result = FighterService.getAll()
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.get('/:id', function (req, res, next) {
    try {
        const result = FighterService.getOne(req.params.id);
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.post('/', createFighterValid, function (req, res, next) {
    try {
        const result = FighterService.create(req.body);
        res.data = result;
        res.statusCode = 200;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.put('/:id', updateFighterValid, function (req, res, next) {
    try {
        const result = FighterService.update(req.params.id, req.body);
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
    try {
        const result = FighterService.delete(req.params.id);
        res.data = result;
    } catch (err) {
        res.err = err.message;
        res.statusCode = err.statusCode;
    } finally {
        next();
    }

}, responseMiddleware);

module.exports = router;