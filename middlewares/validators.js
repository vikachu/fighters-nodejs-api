
class Validators {
    validatePattern = (value, pattern, throwError) => {
        !pattern.test(value) && throwError();
    }

    validateMin = (value, min, throwError) => {
        !(value >= min) && throwError();
    }

    validateMax = (value, max, throwError) => {
        !(value <= max) && throwError();
    }

    validateUnique = (value, fieldName, service, throwError) => {
        (service.search(i => i[fieldName] === value)) && throwError();
    };

    validateExists = (value, fieldName, service, throwError) => {
        !(service.search(i => i[fieldName] === value)) && throwError();
    };

    validateOddFields = (object, fields, throwError) => {
        !Object.keys(object).every(value => fields.includes(value)) && throwError();
    }

    validateMandatoryFieldsPresent = (object, fields, throwError) => {
        !fields.every(value => object.hasOwnProperty(value)) && throwError();
    }

    throwError = (message, statusCode) => {
        const err = new Error(message)
        err.statusCode = statusCode;
        throw err
    }

}


module.exports = new Validators();