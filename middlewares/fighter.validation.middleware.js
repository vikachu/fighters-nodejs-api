const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');
const Validators = require('./validators');

const fighterFields = ["name", "health", "power", "defense"];

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { name, power, defense } = req.body;
    try {
        Validators.validateOddFields(req.body, fighterFields, () => Validators.throwError('Fighter entity to create is not valid: wrong fields.', 400));
        Validators.validateMandatoryFieldsPresent(req.body, fighterFields, () => Validators.throwError('No mandatory fields: name, health, power, defense.', 400));
        Validators.validateUnique(name, 'name', FighterService, () => Validators.throwError('Name already exists.', 400));
        Validators.validateMax(power, 99, () => Validators.throwError('Power should be an integer < 100.', 400));
        Validators.validateMin(defense, 1, () => Validators.throwError('Defense should be an integer > 0 and <= 10.', 400));
        Validators.validateMax(defense, 10, () => Validators.throwError('Defense should be an integer > 0 and <= 10.', 400));
        next();
    } catch (error) {
        res.status(error.statusCode).send({
            error: true,
            message: error.message
        });
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const { name, power, defense } = req.body;
    const { id } = req.params;
    try {
        Validators.validateOddFields(req.body, fighterFields, () => Validators.throwError('Fighter entity to create is not valid: wrong fields.', 400));
        Validators.validateExists(id, 'id', FighterService, () => Validators.throwError('Fighter does not exists.', 404));
        name && Validators.validateUnique(name, 'name', FighterService, () => Validators.throwError('Name already exists.', 400));
        power && Validators.validateMax(power, 99, () => Validators.throwError('Power should be an integer < 100.', 400));
        defense && Validators.validateMin(defense, 1, () => Validators.throwError('Defense should be an integer > 0 and <= 10.', 400));
        defense && Validators.validateMax(defense, 10, () => Validators.throwError('Defense should be an integer > 0 and <= 10.', 400));
        next();
    } catch (error) {
        res.status(error.statusCode).send({
            error: true,
            message: error.message
        });
    }

}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;