const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if(res.err) {
        res.status(res.statusCode).send({
            error: true,
            message: res.err
        });
    } else {
        res.status(res.statusCode).send(res.data);
    }
    next();
}

exports.responseMiddleware = responseMiddleware;