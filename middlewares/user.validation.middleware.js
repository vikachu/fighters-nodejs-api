const { user } = require('../models/user');
const UserService = require('../services/userService');
const Validators = require('./validators');


const emailRegex = /@gmail\.com$/;
const phoneRegex = /^\+380(\d){9}$/;
const userFields = ["email", "password", "firstName", "lastName", "phoneNumber"];

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const { email, phoneNumber, password } = req.body;
    try {
        Validators.validateOddFields(req.body, userFields, () => Validators.throwError('User entity to create is not valid: wrong fields.', 400));
        Validators.validateMandatoryFieldsPresent(req.body, userFields, () => Validators.throwError('No mandatory fields: firstName, lastName, email, phoneNumber, password.', 400));
        Validators.validatePattern(email, emailRegex, () => Validators.throwError('Email should be @gmail.com.', 400));
        Validators.validatePattern(phoneNumber, phoneRegex, () => Validators.throwError('Phone number should start with +380.', 400));
        Validators.validateUnique(email, 'email', UserService, () => Validators.throwError('Email already exists.', 400));
        Validators.validateUnique(phoneNumber, 'phoneNumber', UserService, () => Validators.throwError('Phone number already exists.', 400));
        Validators.validateMin(password.length, 3, () => Validators.throwError('Password should contain more than 3 characters.', 400));
        next();
    } catch (error) {
        res.status(error.statusCode).send({
            error: true,
            message: error.message
        });
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const { email, phoneNumber, password } = req.body;
    const { id } = req.params;
    try {
        Validators.validateOddFields(req.body, userFields, () => Validators.throwError('User entity to create is not valid: wrong fields.', 400));
        Validators.validateExists(id, 'id', UserService, () => Validators.throwError('User does not exists.', 404));
        email && Validators.validatePattern(email, emailRegex, () => Validators.throwError('Email should be @gmail.com.', 400));
        phoneNumber && Validators.validatePattern(phoneNumber, phoneRegex, () => Validators.throwError('Phone number should start with +380.', 400));
        email && Validators.validateUnique(email, 'email', UserService, () => Validators.throwError('Email already exists.', 400));
        phoneNumber && Validators.validateUnique(phoneNumber, 'phoneNumber', UserService, () => Validators.throwError('Phone number already exists.', 400));
        password && Validators.validateMax(password.length, 3, () => Validators.throwError('Password should contain more than 3 characters.', 400));
        next();
    } catch (error) {
        res.status(error.statusCode).send({
            error: true,
            message: error.message
        });
    }

}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

