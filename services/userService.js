const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAll() {
        const users = UserRepository.getAll();
        return users;
    }

    getOne(id) {
        const user = UserRepository.getOne(user => user.id === id);
        if (!user) {
            const err = new Error('User entity not found.')
            err.statusCode = 404;
            throw err;
        }
        return user;
    }

    create(user) {
        const item = UserRepository.create(user);
        if (!item) {
            const err = new Error('Error occured while creating the user.')
            err.statusCode = 400;
            throw err;
        }
        return item;
    }

    update(id, data) {
        const user = UserRepository.update(id, data);
        if (!user) {
            const err = new Error('User entity to update not found.')
            err.statusCode = 404;
            throw err;
        }
        return user;
    }

    delete(id) {
        const user = UserRepository.delete(id);
        if (!user) {
            const err = new Error('User entity to delete not found.')
            err.statusCode = 404;
            throw err;
        }
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();