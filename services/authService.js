const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            const err = new Error('User not found')
            err.statusCode = 404;
            throw err;
        }
        return user;
    }
}

module.exports = new AuthService();