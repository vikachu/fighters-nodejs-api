const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAll() {
        const fighters = FighterRepository.getAll()
        return fighters;
    }

    getOne(id) {
        const fighter = FighterRepository.getOne(fighter => fighter.id === id);
        if (!fighter) {
            const err = new Error('Fighter entity not found.')
            err.statusCode = 404;
            throw err;
        }
        return fighter;
    }

    create(fighter) {
        const item = FighterRepository.create(fighter);
        if (!item) {
            const err = new Error('Error occured while creating the fighter.')
            err.statusCode = 400;
            throw err;
        }
        return item;
    }

    update(id, data) {
        const fighter = FighterRepository.update(id, data);
        if (!fighter) {
            const err = new Error('Fighter entity to update not found.')
            err.statusCode = 404;
            throw err;
        }
        return fighter;
    }

    delete(id) {
        const fighter = FighterRepository.delete(id);
        if (!fighter) {
            const err = new Error('Fighter entity to delete not found.')
            err.statusCode = 404;
            throw err;
        }
        return fighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();